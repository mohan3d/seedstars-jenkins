#!/usr/bin/env python
# coding: utf-8
"""
Jenkins - retrieves and stores list of jobs from a jenkins instance.
Usage:
    jenkins.py -u <username> -p <password> -j <jenkins_instance_url>
    jenkins.py (-h | --help | --usage)

    -h, --help                              Display this message and quit
"""
import sys
import urllib.parse

import docopt
import peewee
import requests

JENKINS_JOBS_RELATIVE_URL = '/api/json?tree=jobs[name,builds[number,building,timestamp]]'

db = peewee.SqliteDatabase('jenkins.db')


# -------------------- Database --------------------
class DBJob(peewee.Model):
    """Job model describes job which will be saved in database."""
    name = peewee.CharField()
    running = peewee.BooleanField()
    timestamp = peewee.TimestampField(null=True)

    class Meta:
        database = db


def db_init():
    """Initializes db, and create tables if not exist."""
    db.connect()
    db.create_tables([DBJob])


def save_job(job):
    """Saves job to database.

    Args:
        job: jenkins Job object.
    """
    dbjob = DBJob(name=job.name, running=job.running, timestamp=job.timestamp)
    dbjob.save()


# -------------------- Jenkins --------------------
class Job:
    def __init__(self, job_info):
        """Initializes Jenkins instance with instance url, username and password.

        Args:
             job_info (dict): all requested job info (number, building and timestamp).

        """

        self._name = job_info.get('name')
        self._builds = job_info.get('builds')
        self._last_build = self._get_last_build

    @property
    def running(self):
        """Return running status of job, True if last build is running,
        False if job is not built yet, or last build completed.

        Returns:
            bool: True if job is running otherwise False.

        """

        last_build = self._last_build
        if last_build:
            return last_build['building']
        return False

    @property
    def timestamp(self):
        """Return timestamp of the last build.

        Returns:
            int: last build timestamp.

        """

        last_build = self._last_build
        if last_build:
            return last_build['timestamp']

    @property
    def name(self):
        return self._name

    @property
    def _get_last_build(self):
        """Return build which has max build-number.

        Returns:
            dict: last build info.

        """

        if not self._builds:
            return None
        return max(self._builds, key=lambda b: b['number'])


class JenkinsException(Exception):
    """Generic exception for response errors."""
    pass


class Jenkins:
    def __init__(self, base_url, username, password):
        """Initializes Jenkins instance with instance url, username and password.

        Args:
             base_url (str): base url for jenkins instance including port.
             username (str): username for jenkins auth.
             password (str): password for jenkins auth.

        """
        self.instance_url = urllib.parse.urljoin(base_url, JENKINS_JOBS_RELATIVE_URL)
        self.username = username
        self.password = password

    def get_jobs(self):
        """Request and parse jobs from the jenkins instance

        Returns:
            list: jobs objects.
        """
        resp = self._get(self.instance_url)
        jobs = resp.json().get('jobs', [])
        return [Job(job_info) for job_info in jobs]

    def _get(self, url):
        """Request url after setting auth and headers.

        Args:
            url (str): url to be requested.

        Raises:
            JenkinsException

        """
        resp = requests.get(url, auth=(self.username, self.password),
                            headers={'Content-Type': 'application/json'})

        if resp.status_code != 200:
            raise JenkinsException(resp.reason)

        return resp


def fetch_and_store(jenkins):
    """Fetch jobs and store them in the db."""
    jobs = jenkins.get_jobs()

    for job in jobs:
        save_job(job)


def main():
    args = docopt.docopt(__doc__, argv=sys.argv[1:], version="0.1")

    try:
        username = args.get('<username>')
        password = args.get('<password>')
        jenkins_url = args.get('<jenkins_instance_url>')

        # Initialize db.
        db_init()

        jenkins = Jenkins(jenkins_url, username, password)
        fetch_and_store(jenkins)

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
